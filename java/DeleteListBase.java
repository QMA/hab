package com.example.qma.householdaccountbook;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * アイテムを削除するダイアログのベースとなるもの
 * Created by QMA on 2015/04/12.
 */
abstract class DeleteListBase {
    /** 表示するアラートダイアログ */
    private AlertDialog dialog;
    /** 選択されたアイテム */
    private ArrayList<Integer> checkedItems;
    /** 表示するアイテム */
    private String[] items;
    private Activity activity;

    /** ダイアログを表示させる */
    DeleteListBase(final Activity activity, final DeleteListInterface delete_interface, final String table_name){
        this.activity = activity;
        callDialog(delete_interface, table_name);
    }

    private void callDialog(final DeleteListInterface delete_interface, final String table_name){
        // アイテムを取得する
        items = delete_interface.getItems();
        checkedItems = new ArrayList<Integer>();

        // ダイアログの生成
        AlertDialog.Builder dlg = new AlertDialog.Builder(activity)
                .setTitle("削除")
                .setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        // チェックされたアイテムの番号を記録する
                        if (isChecked) checkedItems.add(which);
                        else checkedItems.remove((Integer) which);
                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DataBaseController dbc = new DataBaseController(activity);
                        SQLiteDatabase db = dbc.getWritableDatabase();

                        // 残ったアイテムを入れるもの
                        ArrayList<String> leftItems = new ArrayList<String>();

                        for (int i = 0; i < delete_interface.getItems().length; i++) {
                            if (!checkedItems.contains(i)) {
                                leftItems.add(items[i]);
                            } else {
                                deleteItem(activity, db, dbc, items[i]);
                                delete_interface.decreaseNumOfText();
                            }
                        }

                        delete_interface.setItems(dbc.getElementsFromTable(db, table_name));
                        db.close();
                    }
                })
                .setNegativeButton("Cancel", null);

        dialog = dlg.show();
    }

    /***
     * データベースから指定のアイテムを削除する
     * @param activity :アクティビティ
     * @param db :データベース
     * @param item :削除するアイテム、アイテムの組み合わせ
     */
    abstract void deleteItem(Activity activity, SQLiteDatabase db, DataBaseController dbc, String item);
}
