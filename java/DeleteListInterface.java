package com.example.qma.householdaccountbook;

/**
 * Created by QMA on 2015/04/12.
 */
public interface DeleteListInterface {
    String[] getItems();
    void setItems(String[][] items);
    void decreaseNumOfText();
}
