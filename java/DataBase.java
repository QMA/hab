package com.example.qma.householdaccountbook;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * DataBase reference:
 *  history {_id(integer), year(integer), month(integer), day(integer), kind(text), money(integer)}
 *  kind {_id(integer), kind(text)}
 *  favorite {_id(integer), kind(text), money(integer)}
 *
 * Created by QMA on 2015/04/09.
 */
public class DataBase  extends SQLiteOpenHelper {
    /** 記録されている年(西暦) */
    public static final String RECODED_YEAR     = "year";
    /** 記録されている月 */
    public static final String RECODED_MONTH    = "month";
    /** 記録されている日にち */
    public static final String RECODED_DAY      = "day";
    /** 記録されている種類 */
    public static final String RECODED_KIND     = "kind";
    /** 記録されている金額 */
    public static final String RECODED_MONEY    = "money";

    /** 履歴テーブル */
    public static final String HISTORY  = "history";
    /** 種類テーブル */
    public static final String KIND     = "kind";
    /** お気に入りテーブル */
    public static final String FAVORITE = "favorite";
    /** DataBase名称 */
    private static final String DB_NAME = "HOUSEHOLD_ACCOUNT_BOOK_QMA_DB";

    /**
     * コンストラクタ
     */
    public DataBase(Context context) {
        super(context, DB_NAME, null, 1);
    }

    /**
     * データベース生成
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();

        try {
            // 履歴
            StringBuilder createSql = new StringBuilder();
            createSql.append("create table " + HISTORY + " (");
            createSql.append("_id integer primary key autoincrement, ");
            createSql.append(RECODED_YEAR + " integer not null, ");
            createSql.append(RECODED_MONTH + " integer not null, ");
            createSql.append(RECODED_DAY + " integer not null, ");
            createSql.append(RECODED_KIND + " text not null, ");
            createSql.append(RECODED_MONEY + " integer not null");
            createSql.append(");");
            db.execSQL(createSql.toString());

            // 種類
            createSql = new StringBuilder();
            createSql.append("create table " + KIND + " (");
            createSql.append("_id integer primary key autoincrement, ");
            createSql.append(RECODED_KIND + " text not null");
            createSql.append(");");
            db.execSQL(createSql.toString());

            // お気に入り
            createSql = new StringBuilder();
            createSql.append("create table " + FAVORITE + " (");
            createSql.append("_id integer primary key autoincrement, ");
            createSql.append(RECODED_KIND + " text not null, ");
            createSql.append(RECODED_MONEY + " integer not null");
            createSql.append(");");
            db.execSQL(createSql.toString());

            initSetting(db);

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    /**
     * データベースの更新
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    /***
     * 最初にデータベースへ追加するアイテム
     * @param db :データベース
     */
    private void initSetting(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(RECODED_KIND, "sample1");
        values.put(RECODED_MONEY, -1000);
        db.insert(FAVORITE, null, values);

        values.clear();
        values.put(RECODED_KIND, "sample2");
        values.put(RECODED_MONEY, -2000);
        db.insert(FAVORITE, null, values);

        values.clear();
        values.put(RECODED_KIND, "sample1");
        db.insert(KIND, null, values);

        values.clear();
        values.put(RECODED_KIND, "sample2");
        db.insert(KIND, null, values);
    }
}
