package com.example.qma.householdaccountbook;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;

/**
 * 金額や種類を登録するクラス
 * Created by QMA on 2015/04/04.
 */
public class Input {
    /** 日付を表示するビュー */
    private TextView view_date_result;
    /** 金額を表示するビュー */
    private TextView view_money_result;
    /** 種類を表示するビュー */
    private TextView view_kind_result;

    /***
     * それぞれのビューに機能を設定し、ダイアログを表示する
     * @param activity :アクティビティ
     */
    Input(final Activity activity){

        // カスタムビューを設定
        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.input, (ViewGroup)activity.findViewById(R.id.input));

        // ダイアログを表示する
        callDialog(activity, layout);

        // 金額の表示
        view_money_result = (TextView)(layout.findViewById(R.id.view_money_result));
        Button money  = (Button)(layout.findViewById(R.id.money));
        money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new InputNumber(activity, view_money_result);
            }
        });

        // 種類の表示
        view_kind_result = (TextView)(layout.findViewById(R.id.view_kind_result));
        Button kind  = (Button)(layout.findViewById(R.id.kind));
        kind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new InputKind(activity, view_kind_result);
            }
        });

        // 登録されている件数の表示
        final TextView view_favorite_result = (TextView)(layout.findViewById(R.id.view_favorite_result));
        view_favorite_result.setText(getFavoriteNum(activity));

        // お気に入り
        Button favorite  = (Button)(layout.findViewById(R.id.favorite));
        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new InputFavorite(activity, view_money_result, view_kind_result, view_favorite_result);
            }
        });

        // 日付
        Button date = (Button)(layout.findViewById(R.id.date));
        view_date_result = (TextView)(layout.findViewById(R.id.view_date_result));
        view_date_result.setText(getNowDate());
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new InputDate(activity, view_date_result);
            }
        });
    }

    /***
     * お気に入りの件数を返す
     * @param activity :アクティビティ
     * @return お気に入りの件数
     */
    private String getFavoriteNum(Activity activity){
        DataBaseController dbc = new DataBaseController(activity);
        SQLiteDatabase db = dbc.getWritableDatabase();
        int num = dbc.getElementsFromTable(db, DataBase.FAVORITE).length;
        db.close();

        return String.valueOf(num) + "件登録";
    }

    /***
     * 履歴に追加する
     * @param activity :アクティビティ
     */
    private void addHistory(final Activity activity){
        if (
                !view_kind_result.getText().toString().isEmpty() &&
                        !view_money_result.getText().toString().replaceAll("[+ | -]","").equals("円")
                ) {
            DataBaseController dbc = new DataBaseController(activity);
            SQLiteDatabase db = dbc.getWritableDatabase();
            // 数字とマイナス以外を抜き取りint型へ
            int money = Integer.parseInt(
                    view_money_result.getText().toString().replaceAll("[^0-9 & ^-]","")
            );
            // 履歴に追加する
            String[] dates = view_date_result.getText().toString().split("/", 0);
            dbc.insertIntoHistory(
                    activity,
                    db,
                    Integer.parseInt(dates[0]),
                    Integer.parseInt(dates[1]),
                    Integer.parseInt(dates[2]),
                    view_kind_result.getText().toString(),
                    money);
            db.close();
        }
    }

    /**
     * 現在時刻を得る
     * @return :year/month/day
     */
    private String getNowDate(){
        String date;
        Calendar cal = Calendar.getInstance();

        date = cal.get(Calendar.YEAR) + "/" +
                (cal.get(Calendar.MONTH) + 1) + "/" +
                cal.get(Calendar.DATE);

        return date;
    }

    /***
     * ダイアログの表示
     * @param activity :アクティビティ
     * @param layout :ダイアログで使用するレイアウト
     */
    private void callDialog(final Activity activity, View layout){
        // 入力フォーマットの表示
        AlertDialog.Builder listDlg = new AlertDialog.Builder(activity)
                .setTitle("登録")
                .setView(layout)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                addHistory(activity);
                            }
                        })
                .setNegativeButton("キャンセル",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
        listDlg.create().show();
    }
}
