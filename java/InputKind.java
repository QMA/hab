package com.example.qma.householdaccountbook;

import android.app.Activity;
import android.app.AlertDialog;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 種類を選ばせるクラス
 * Created by QMA on 2015/04/12.
 */
public class InputKind extends ListLayoutBase {
    /** アイテム */
    private String[] items;

    /**
     * アラートダイアログの表示
     * @param activity :アクティビティ
     * @param result   :選択したものを表示するためのビュー
     */
    public InputKind(Activity activity, TextView result) {
        super(activity, result, "", DataBase.KIND);
        getDialog().setTitle("種類" + "(" + entriedNum(activity) + "/10)");
    }

    /***
     * アイテムをリストビューにいれる
     * @param items :表示するアイテム
     */
    @Override
    public void setItems(String[][] items){
        ArrayList<String> list = new ArrayList<>();

        getAdapter().clear();
        for(int i = 0; i < items.length; i++) {
            getAdapter().add(items[i][1]);
            list.add(items[i][1]);
        }

        this.items = Arrays.copyOf(list.toArray(), list.toArray().length, String[].class);
    }

    @Override
    public String[] getItems() {
        return items;
    }

    @Override
    public void decreaseNumOfText() { }

    /***
     * アイテムを削除するためのダイアログを表示する
     * @param activity
     */
    @Override
    protected void callDeleteFunction(Activity activity) {
        new DeleteKind(activity, this);
    }

    /***
     * アイテムを追加するためのダイアログを表示する
     * @param activity
     */
    @Override
    protected void callEntryFunction(Activity activity) {
        if(isRegisterable(activity)) {
            new EntryKind(activity, this);
        }
    }

    /***
     * データベースに登録されている種類の数を返す
     * @param activity :アクティビティ
     * @return 登録されている種類の数
     */
    private int entriedNum(Activity activity){
        DataBaseController dbc = new DataBaseController(activity);
        SQLiteDatabase db = dbc.getReadableDatabase();
        int num = dbc.getElementsFromTable(db, DataBase.KIND).length;
        db.close();

        return num;
    }

    /***
     * 登録数の上限を超えているかどうか
     * @param activity :アクティビティ
     * @return :上限を超えていなければtrue
     */
    public boolean isRegisterable(Activity activity){
        // 登録上限数
        final int MAX_REGISTRATION = 10;

        if(entriedNum(activity) > MAX_REGISTRATION - 1){
            Toast.makeText(activity, MAX_REGISTRATION + "以上は登録できません", Toast.LENGTH_LONG).show();
            return false;
        }
        else return true;
    }
}
