package com.example.qma.householdaccountbook;

import android.app.Activity;
import android.app.AlertDialog;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * アイテムをリスト形式で表示し、
 * 選択したアイテムを元のビューに表示させる。
 * Created by QMA on 2015/04/07.
 */
abstract class ListLayoutBase implements DeleteListInterface{
    /** 表示するアラートダイアログ */
    private AlertDialog dialog;
    /** キャンセルボタン */
    private Button btn_cansel;
    /** 登録ボタン */
    private Button btn_entry;
    /** 消去ボタン */
    private Button btn_delete;
    /** アダプター */
    private ArrayAdapter<String> adapter;
    /** リストビュー */
    private ListView listView;

    /***
     * アラートダイアログの表示。
     * 1つのビューを操作するためのコンストラクタ
     * @param activity :アクティビティ
     * @param result :選択したものを表示するためのビュー
     * @param title :ダイアログのタイトル
     * @param table_name :データベース中の、操作したいテーブル
     */
    ListLayoutBase(final Activity activity, final TextView result, final String title, final String table_name){
        initSetting(activity, title, table_name);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListView listView = (ListView) parent;
                // クリックされたアイテムを取得する
                result.setText((String) listView.getItemAtPosition(position));
                dialog.dismiss();
                dialog = null;
            }
        });
    }

    /***
     * アラートダイアログの表示。
     * 2つのビューを操作するためのコンストラクタ
     * @param activity :アクティビティ
     * @param money_result :金額を表示するためのビュー
     * @param kind_result :種類を表示するためのビュー
     * @param title :ダイアログのタイトル
     * @param table_name :データベース中の、操作したいテーブル
     */
    ListLayoutBase(final Activity activity, final TextView money_result, final TextView kind_result, final String title, final String table_name) {
        initSetting(activity, title, table_name);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListView listView = (ListView) parent;
                // クリックされたアイテムを取得する
                String str = (String) listView.getItemAtPosition(position);
                String[] items = str.split("\n", 0);

                kind_result.setText(items[0]);
                money_result.setText(items[1]);
                dialog.dismiss();
                dialog = null;
            }
        });
    }

    /***
     * 2つのコンストラクタの共通の初期設定
     * @param activity :アクティビティ
     * @param title :ダイアログのタイトル
     * @param table_name :操作するテーブルの名前
     */
    private void initSetting(final Activity activity, final String title, final String table_name){
        adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1);

        // データベースからアイテムの取得
        setItems(callDataBase(activity, table_name));

        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.list_layout_base, (ViewGroup)activity.findViewById(R.id.list_layout_base));

        // リストビューの設定
        listView = (ListView) layout.findViewById(R.id.list_base);
        listView.setAdapter(adapter);

        // ボタンの初期設定
        initButton(activity, layout);

        AlertDialog.Builder dlg = new AlertDialog.Builder(activity)
                .setTitle(title)
                .setView(layout);

        dialog = dlg.show();
    }

    /***
     * ボタンの初期設定
     * @param activity
     * @param layout
     */
    private void initButton(final Activity activity, View layout){
        // キャンセルボタン
        btn_cansel = (Button)layout.findViewById(R.id.cansel_base);
        btn_cansel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dialog = null;
            }
        });

        // 登録してあるものを削除するボタン
        btn_delete = (Button)layout.findViewById(R.id.delete_base);
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callDeleteFunction(activity);
            }
        });

        // 新たに種類を登録するボタン
        btn_entry = (Button)layout.findViewById(R.id.entry_base);
        btn_entry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callEntryFunction(activity);
            }
        });
    }

    /** アダプターを得る */
    public ArrayAdapter getAdapter(){ return adapter; }

    /***
     * データベースからアイテムを取り出す
     * @param activity
     * @return
     */
    private String[][] callDataBase(Activity activity, final String table_name){
        DataBaseController dbc = new DataBaseController(activity);
        SQLiteDatabase db = dbc.getReadableDatabase();
        String[][] items = dbc.getElementsFromTable(db, table_name);
        db.close();

        return items;
    }

    protected AlertDialog getDialog(){
        return dialog;
    }

    /***
     * リストへアイテムをセットする
     * @param items
     */
    public abstract void setItems(String[][] items);
    /** アイテムを得る */
    public abstract String[] getItems();
    protected abstract void callDeleteFunction(Activity activity);
    protected abstract void callEntryFunction(Activity activity);
}
