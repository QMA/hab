package com.example.qma.householdaccountbook;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

/**
 * Created by QMA on 2015/04/12.
 */
public class DeleteFavorite extends DeleteListBase{

    /**
     * ダイアログを表示させる
     *
     * @param activity
     * @param delete_interface
     */
    DeleteFavorite(Activity activity, final DeleteListInterface delete_interface) {
        super(activity, delete_interface, DataBase.FAVORITE);
    }

    /***
     * データベースからアイテムを削除する
     * @param activity :アクティビティ
     * @param db :データベース
     * @param dbc :データベースの制御
     * @param item :削除するアイテム、アイテムの組み合わせ
     */
    @Override
    void deleteItem(Activity activity, SQLiteDatabase db, DataBaseController dbc, String item) {
        String[] items = item.split("\n", 0);
        String id = dbc.getIdOfFavorite(db, items[0], items[1].replaceAll("[^0-9 & ^-]",""));
        if(id != null) {
            dbc.deleteFromTable(activity, db, DataBase.FAVORITE, id);
        }
        else{
            Toast.makeText(activity, "アイテムが存在しませんでした", Toast.LENGTH_SHORT).show();
        }
    }
}
