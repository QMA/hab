package com.example.qma.householdaccountbook;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * アラートダイアログで数値入力を行うクラス
 * Created by QMA on 2015/04/07.
 */
public class InputNumber {
    /** 入力した数値の表示 */
    private TextView result;

    /**
     * 各種ボタンの設定後アラートダイアログを表示する。
     * @param activity :アクティビティ
     * @param view_money_result :最終的な結果を入れるためのビュー
     */
    InputNumber(Activity activity, final TextView view_money_result){
        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.input_number, (ViewGroup)activity.findViewById(R.id.input_number));

        // 現在入力されている金額を表示するビュー
        result = (TextView)layout.findViewById(R.id.view_money);

        // 各種ボタンの設定
        setButtonNumber(layout);
        setButtonClear(layout);
        setButtonChange(layout);

        // ダイアログの呼び出し
        callDialog(activity, layout, view_money_result);
    }

    /***
     * 数字ボタンの設定
     * @param layout :ダイアログで使用するレイアウト
     */
    private void setButtonNumber(View layout){
        Button btn[] = new Button[10];

        // ここを簡単にできないものか...
        btn[0] = (Button)layout.findViewById(R.id.btn_0);
        btn[1] = (Button)layout.findViewById(R.id.btn_1);
        btn[2] = (Button)layout.findViewById(R.id.btn_2);
        btn[3] = (Button)layout.findViewById(R.id.btn_3);
        btn[4] = (Button)layout.findViewById(R.id.btn_4);
        btn[5] = (Button)layout.findViewById(R.id.btn_5);
        btn[6] = (Button)layout.findViewById(R.id.btn_6);
        btn[7] = (Button)layout.findViewById(R.id.btn_7);
        btn[8] = (Button)layout.findViewById(R.id.btn_8);
        btn[9] = (Button)layout.findViewById(R.id.btn_9);

        // リスナーの追加
        for(int i = 0; i < btn.length; i++) {
            btn[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isInputable(v)) {
                        result.setText(
                                result.getText().subSequence(0, result.getText().length() - 1)
                                        + String.valueOf(((Button) v).getText())
                                        + "円"
                        );
                    }
                }
            });
        }
    }

    /***
     * 1文字消去ボタンの設定
     * @param layout :ダイアログで使用するレイアウト
     */
    private void setButtonClear(View layout){
        // １文字消去ボタンの登録
        Button btn = (Button)layout.findViewById(R.id.btn_clear);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (result.getText().length() >= 3) {
                    result.setText(
                            result.getText().subSequence(0, result.getText().length() - 2)
                                    + "円"
                    );
                }
            }
        });
    }

    /***
     * 収入・支出を切り替えるボタンの設定
     * @param layout :ダイアログで使用するレイアウト
     */
    private void setButtonChange(View layout){
        // 収入・支出の切り替え
        final Button btn = (Button)layout.findViewById(R.id.btn_change);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn.getText().toString().equals("収入")) {
                    result.setText(
                            "+" +
                                    result.getText().subSequence(1, result.getText().length() - 1) +
                                    "円"
                    );
                    btn.setText("支出");
                }
                else{
                    result.setText(
                            "-" +
                                    result.getText().subSequence(1, result.getText().length() - 1) +
                                    "円"
                    );
                    btn.setText("収入");
                }
            }
        });
    }

    /***
     * ダイアログを表示する
     * @param activity :アクティビティ
     * @param layout :ダイアログで使用するレイアウト
     * @param view_money_result :金額を表示するビュー
     */
    private void callDialog(Activity activity, View layout, final TextView view_money_result){
        AlertDialog.Builder dlg = new AlertDialog.Builder(activity)
                .setTitle("金額の入力")
                .setView(layout)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                view_money_result.setText(result.getText());
                                dialog.dismiss();
                                dialog = null;
                            }
                        })
                .setNegativeButton("キャンセル",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });

        //　ダイアログの表示
        dlg.show();
    }

    /***
     * 押された数字が入力できるかどうか
     * @param v :押されたボタン
     * @return :入力可能な数字だとtrue
     */
    private boolean isInputable(View v){
        String str = result.getText().toString().replaceAll("[+ | -]", "");

        if(str.equals("円") && v.getId() == R.id.btn_0) return false;
        else return true;
    }
}
