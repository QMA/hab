package com.example.qma.householdaccountbook;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

/**
 * Created by QMA on 2015/04/16.
 */
public class InputDate {
    /** 表示するダイアログ */
    private AlertDialog.Builder listDlg;

    InputDate(Activity activity, final TextView date){
        final DatePicker datePicker = new DatePicker(activity);
        datePicker.setCalendarViewShown(false);

        listDlg = new AlertDialog.Builder(activity)
                .setTitle("日付")
                .setView(datePicker)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                date.setText(
                                        datePicker.getYear() + "/" +
                                        (datePicker.getMonth() + 1) + "/" +
                                        datePicker.getDayOfMonth()
                                );
                            }
                        })
                .setNegativeButton("キャンセル",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
        listDlg.create().show();
    }
}
