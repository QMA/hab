package com.example.qma.householdaccountbook;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * アイテムを新たに登録するクラス
 * Created by QMA on 2015/04/08.
 */
public class EntryKind {
    /** 表示するアラートダイアログ */
    private AlertDialog dialog;
    /** 入力を受け付けるテキスト */
    private TextView entry_text;

    /***
     * ダイアログを表示する
     * @param activity :アクティビティ
     * @param input_kind :このクラスを使うクラス
     */
    EntryKind(final Activity activity, final InputKind input_kind) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.intput_kind_entry, (ViewGroup) activity.findViewById(R.id.input_kind_entry));

        entry_text = (TextView) layout.findViewById(R.id.entry_kind);

        AlertDialog.Builder dlg = new AlertDialog.Builder(activity)
                .setTitle("種類の登録")
                .setView(layout)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // データベースを操作
                                addKind(activity, input_kind);
                            }
                        })
                .setNegativeButton("キャンセル",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                dialog = null;
                            }
                        });

        dialog = dlg.show();
    }

    /***
     * データベースに種類の追加
     * @param activity :アクティビティ
     * @param input_kind :アイテムをリスト形式で表示するためのクラス
     */
    private void addKind(Activity activity, final InputKind input_kind){
        if(!entry_text.getText().toString().equals("")) {
            DataBaseController dbc = new DataBaseController(activity);
            SQLiteDatabase db = dbc.getWritableDatabase();

            if(input_kind.isRegisterable(activity)) {
                dbc.insertIntoKind(activity, db, entry_text.getText().toString());
                input_kind.setItems(dbc.getElementsOfKind(db));
            }

            db.close();
        }
    }
}
