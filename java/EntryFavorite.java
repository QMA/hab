package com.example.qma.householdaccountbook;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;

/**
 * お気に入りを新規登録する
 * Created by QMA on 2015/04/12.
 */
public class EntryFavorite{
    /** 表示するダイアログ */
    private AlertDialog.Builder listDlg;
    /** 金額を表示するビュー */
    private TextView view_money_result;
    /** 種類を表示するビュー */
    private TextView view_kind_result;

    /***
     * アラートダイアログを表示する
     * @param activity :アクティビティ
     */
    EntryFavorite(final Activity activity, final InputFavorite input_favorite){

        // カスタムビューを設定
        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.input, (ViewGroup)activity.findViewById(R.id.input));

        // 入力フォーマットの表示
        listDlg = new AlertDialog.Builder(activity)
                .setTitle("登録")
                .setView(layout)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                addFavorite(activity, input_favorite);
                                input_favorite.increaseNumOfText();
                            }
                        })
                .setNegativeButton("キャンセル",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
        listDlg.create().show();

        // 金額の表示
        view_money_result = (TextView)(layout.findViewById(R.id.view_money_result));
        Button money  = (Button)(layout.findViewById(R.id.money));
        money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new InputNumber(activity, view_money_result);
            }
        });

        // 種類の表示
        view_kind_result = (TextView)(layout.findViewById(R.id.view_kind_result));
        Button kind  = (Button)(layout.findViewById(R.id.kind));
        kind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new InputKind(activity, view_kind_result);
            }
        });

        // "いつもの"・"日付"を非表示にする
        RelativeLayout favorite = (RelativeLayout)(layout.findViewById(R.id.layout_favorite));
        favorite.setVisibility(View.GONE);
        RelativeLayout date = (RelativeLayout)(layout.findViewById(R.id.layout_date));
        date.setVisibility(View.GONE);
    }

    /***
     * データベースへお気に入りを追加する
     * @param activity :アクティビティ
     * @param input_favorite :お気に入りを表示するビュー
     */
    private void addFavorite(Activity activity, final InputFavorite input_favorite){
        if (
                !view_kind_result.getText().toString().isEmpty() &&
                        !view_money_result.getText().toString().equals("円")
                ) {
            int money = Integer.parseInt(
                    view_money_result.getText().toString().replaceAll("[^0-9 & ^-]","")
            );
            DataBaseController dbc = new DataBaseController(activity);
            SQLiteDatabase db = dbc.getReadableDatabase();
            dbc.insertIntoFavorite(activity, db, view_kind_result.getText().toString(), money);
            input_favorite.setItems(dbc.getElementsOfFavorite(db));
            db.close();
        }
    }
}
