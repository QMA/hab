package com.example.qma.householdaccountbook;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * 種類を選択して消去するクラス
 * Created by QMA on 2015/04/08.
 */
public class DeleteKind extends DeleteListBase{

    /**
     * ダイアログを表示させる
     *
     * @param activity
     * @param delete_interface
     */
    DeleteKind(Activity activity, final DeleteListInterface delete_interface) {
        super(activity, delete_interface, DataBase.KIND);
    }

    /***
     * データベースからアイテムを削除する
     * @param activity :アクティビティ
     * @param db :データベース
     * @param dbc :データベースの制御
     * @param item :削除するアイテム、アイテムの組み合わせ
     */
    @Override
    void deleteItem(Activity activity, SQLiteDatabase db, DataBaseController dbc, String item) {
        String id = dbc.getIdOfKind(db, item);
        if(id != null) {
            dbc.deleteFromTable(activity, db, DataBase.KIND, id);
        }
        else{
            Toast.makeText(activity, "アイテムが存在しませんでした", Toast.LENGTH_SHORT).show();
        }
    }
}
