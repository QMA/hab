package com.example.qma.householdaccountbook;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * データベースを操作するクラス
 * <使い方>
 * DataBaseController dbc = new DataBaseController(Activity);
 * SQLiteDatabase db = dbc.getWritableDatabase();
 * ...
 * dbc.(このクラスのメソッド);
 * ...
 * dbc.close();// 最後に必ず呼ぶこと
 *
 * Created by QMA on 2015/04/10.
 */
public class DataBaseController extends DataBase {
    /**
     * コンストラクタ
     * @param context
     */
    public DataBaseController(Context context) {
        super(context);
    }

    /***
     * "履歴"テーブルにタプルを追加
     * @param activity :アクティビティ
     * @param db :データベース
     * @param year :追加した年
     * @param month :追加した月
     * @param day :追加した日
     * @param kind :種類
     * @param money :金額
     */
    public void insertIntoHistory(Activity activity, SQLiteDatabase db, int year, int month, int day, String kind, int money) {
        ContentValues values = new ContentValues();
        values.put(RECODED_YEAR, year);
        values.put(RECODED_MONTH, month);
        values.put(RECODED_DAY, day);
        values.put(RECODED_KIND, kind);
        values.put(RECODED_MONEY, money);

        long ret;
        try {
            db.beginTransaction();
            ret = db.insert(HISTORY, null, values);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        if (ret == -1) {
            Toast.makeText(activity, "追加に失敗しました", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(activity, "履歴に追加しました", Toast.LENGTH_SHORT).show();
        }
    }

    /***
     * "種類"テーブルにタプルを追加
     * @param activity :アクティビティ
     * @param db :データベース
     * @param kind :種類
     */
    public void insertIntoKind(Activity activity, SQLiteDatabase db, String kind) {
        ContentValues values = new ContentValues();
        values.put(RECODED_KIND, kind);

        long ret;
        try {
            db.beginTransaction();
            ret = db.insert(KIND, null, values);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        if (ret == -1) {
            Toast.makeText(activity, "追加に失敗しました", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(activity, "種類に追加しました", Toast.LENGTH_SHORT).show();
        }
    }

    /***
     * "お気に入り"テーブルにタプルを追加
     * @param activity :アクティビティ
     * @param db :データベース
     * @param kind :種類
     * @param money :金額
     */
    public void insertIntoFavorite(Activity activity, SQLiteDatabase db, String kind, int money) {
        ContentValues values = new ContentValues();
        values.put(RECODED_KIND, kind);
        values.put(RECODED_MONEY, money);

        long ret;
        try {
            db.beginTransaction();
            ret = db.insert(FAVORITE, null, values);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        if (ret == -1) {
            Toast.makeText(activity, "追加に失敗しました", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(activity, "お気に入りに追加しました", Toast.LENGTH_SHORT).show();
        }
    }

    /***
     * 指定したテーブルから指定のidを持つタプルを削除する
     * @param activity :アクティビティ
     * @param db :データベース
     * @param table_name :テーブル名
     * @param id :id
     */
    public void deleteFromTable(Activity activity, SQLiteDatabase db,String table_name, String id){
        int ret;
        try {
            db.beginTransaction();
            ret = db.delete(table_name, "_id=?", new String[]{id});
            db.setTransactionSuccessful();
        } catch(Exception e){
            ret = -1;
        }finally {
            db.endTransaction();
        }
        if (ret == -1) {
            Toast.makeText(activity, "削除に失敗しました", Toast.LENGTH_SHORT).show();
        }
    }

    /***
     * 履歴を全件取得する
     * @param db :データベース
     * @return id, 年, 月, 日, 種類, 金額
     */
    public String[][] getElementsOfHistory(SQLiteDatabase db){
        String[][] array = null;

        //SQL作成
        String sql = "select * from " + HISTORY;

        Cursor cursor;
        try {
            db.beginTransaction();
            cursor = db.rawQuery(sql, null);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        if(cursor != null){
            array = new String[cursor.getCount()][cursor.getColumnCount()];
            while (cursor.moveToNext()){
                ArrayList<String> list = new ArrayList();
                list.add(cursor.getString(0));
                list.add(cursor.getString(1));
                list.add(cursor.getString(2));
                list.add(cursor.getString(3));
                list.add(cursor.getString(4));
                list.add(cursor.getString(5));

                array[cursor.getPosition()] = list.toArray(new String[list.size()]);
            }
        }
        return array;
    }

    /***
     * 種類を全件取得する
     * @param db :データベース
     * @return id, 種類
     */
    public String[][] getElementsOfKind(SQLiteDatabase db){
        String[][] array = null;

        //SQL作成
        String sql = "select * from " + KIND;

        Cursor cursor;
        try {
            db.beginTransaction();
            cursor = db.rawQuery(sql, null);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        if(cursor != null){
            array = new String[cursor.getCount()][cursor.getColumnCount()];
            while (cursor.moveToNext()){
                ArrayList<String> list = new ArrayList();
                list.add(cursor.getString(0));
                list.add(cursor.getString(1));

                array[cursor.getPosition()] = list.toArray(new String[list.size()]);
            }
        }
        return array;
    }

    /***
     * お気に入りを全件取得する
     * @param db :データベース
     * @return id, 種類, 金額
     */
    public String[][] getElementsOfFavorite(SQLiteDatabase db){
        String[][] array = null;

        //SQL作成
        String sql = "select * from " + FAVORITE;

        Cursor cursor;
        try {
            db.beginTransaction();
            cursor = db.rawQuery(sql, null);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        if(cursor != null){
            array = new String[cursor.getCount()][cursor.getColumnCount()];
            while (cursor.moveToNext()){
                ArrayList<String> list = new ArrayList();
                for(int i = 0; i < cursor.getColumnCount(); i++) {
                    list.add(cursor.getString(i));
                }
                list.add(cursor.getString(0));
                list.add(cursor.getString(1));
                list.add(cursor.getString(2));

                array[cursor.getPosition()] = list.toArray(new String[list.size()]);
            }
        }
        return array;
    }

    public String[][] getElementsFromTable(SQLiteDatabase db, String table_name){
        String[][] array = null;

        //SQL作成
        String sql = "select * from " + table_name;

        Cursor cursor;
        try {
            db.beginTransaction();
            cursor = db.rawQuery(sql, null);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        if(cursor != null){
            array = new String[cursor.getCount()][cursor.getColumnCount()];
            while (cursor.moveToNext()){
                ArrayList<String> list = new ArrayList();
                for(int i = 0; i < cursor.getColumnCount(); i++) {
                    list.add(cursor.getString(i));
                }

                array[cursor.getPosition()] = list.toArray(new String[list.size()]);
            }
        }
        return array;
    }

    /***
     * 種類に紐づけられているidを取得する
     * @param db :データベース
     * @param kind :種類
     * @return id(検索文字にヒットしないとnullを返す)
     */
    public String getIdOfKind(SQLiteDatabase db, String kind){
        String sql = "select _id from " + KIND + " where " + RECODED_KIND + " = ?;";
        String[] str = {kind};

        return getOneWord(db, sql, str);
    }

    /***
     * 種類と金額からidを取得する
     * @param db :データベース
     * @param kind :種類
     * @param money :金額
     * @return id(検索文字にヒットしないとnullを返す)
     */
    public String getIdOfFavorite(SQLiteDatabase db, String kind, String money){
        String sql =
                "select _id from " + FAVORITE +
                " where " + RECODED_KIND + " = ?" +
                " and " + RECODED_MONEY + " = ?;";
        String[] str = {kind, money};

        return getOneWord(db, sql, str);
    }

    /***
     * 与えられたsql文を実行し、1語だけ返す。
     * @param db :データベース
     * @param sql :実行するsql文
     * @param selection_args :sql文の中に?があればそれを置換する
     * @return 検索にヒットした語を返す(検索文字にヒットしないとnullを返す)
     */
    private String getOneWord(SQLiteDatabase db, String sql, String[] selection_args){
        Cursor cursor = null;

        try {
            db.beginTransaction();
            cursor = db.rawQuery(sql, selection_args);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        if(cursor != null && cursor.getCount() != 0) {
            cursor.moveToNext();
            return String.valueOf(cursor.getString(0));
        }
        else {
            return null;
        }
    }
}
