package com.example.qma.householdaccountbook;

import android.app.Activity;
import android.app.AlertDialog;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * お気に入り一覧から金額と種類の組み合わせを選ばせる
 * Created by QMA on 2015/04/09.
 */
public class InputFavorite extends ListLayoutBase {
    /** お気に入り件数を表示するビュー */
    private TextView view_favorite_result;
    /** アイテム */
    private String[] items;

    /***
     * アラートダイアログ
     * @param activity :アクティビティ
     * @param money_result :金額を表示するテキスト
     * @param kind_result :種類を表示するテキスト
     */
    public InputFavorite(Activity activity, TextView money_result, TextView kind_result, TextView favorite_result) {
        super(activity, money_result, kind_result, "いつもの", DataBase.FAVORITE);
        view_favorite_result = favorite_result;
    }

    /***
     * リストビューにアイテムを設置する
     * @param items
     */
    @Override
    public void setItems(String[][] items){
        getAdapter().clear();
        ArrayList<String> list = new ArrayList<>();
        
        for(int i = 0; i < items.length; i++) {
            int money = Integer.parseInt(items[i][2]);
            String str;

            if(money < 0){
                getAdapter().add(items[i][1] + "\n" + items[i][2] + "円");
                str = items[i][1] + "\n" + items[i][2] + "円";
            }
            else {
                getAdapter().add(items[i][1] + "\n+" + items[i][2] + "円");
                str = items[i][1] + "\n" + items[i][2] + "円";
            }
            list.add(str);
        }

        this.items = Arrays.copyOf(list.toArray(), list.toArray().length, String[].class);
    }

    @Override
    public String[] getItems() {
        return items;
    }

    /***
     * アイテムを削除するためのダイアログを表示する
     * @param activity
     */
    @Override
    protected void callDeleteFunction(Activity activity) {
        new DeleteFavorite(activity, this);
    }

    /***
     * アイテムを追加するためのダイアログを表示する
     * @param activity
     */
    @Override
    protected void callEntryFunction(Activity activity) {
        new EntryFavorite(activity, this);
    }

    /***
     * 登録件数を減らす
     */
    public void decreaseNumOfText(){
        String str = view_favorite_result.getText().toString().replaceAll("[^0-9]","");
        int num = Integer.parseInt(str);
        num--;
        view_favorite_result.setText(String.valueOf(num) + "件登録");
    }

    /***
     * 登録件数を増やす
     */
    protected void increaseNumOfText(){
        String str = view_favorite_result.getText().toString().replaceAll("[^0-9]","");
        int num = Integer.parseInt(str);
        num++;
        view_favorite_result.setText(String.valueOf(num) + "件登録");
    }
}
